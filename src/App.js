import React from 'react';

import Counter from './common/Container1/Counter';
import ActionBar from './common/Container2/ActionBar';

function App() {
  return (
    <div>
      <Counter></Counter>
      <ActionBar></ActionBar>
    </div>
  );
}

export default App;
