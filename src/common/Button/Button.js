import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Button extends Component {
  render() {
    return (
      <button onClick={this.props.onAction}>
        {this.props.children}
      </button>
    );
  }
}

Button.propTypes = {
  onAction: PropTypes.func.isRequired
};

export default Button;