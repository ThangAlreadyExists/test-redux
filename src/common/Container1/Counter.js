import React, { Component } from 'react';
import Store from '../../store';

class Counter extends Component {
  constructor(props) {
    super(props);
    this.store = Store.getInstance();
    this.state = {
      value: this.store.getState()
    }
    this.store.subscribe(() => {
      this.setState({
        value: this.store.getState()
      })
    });
  }
  render() {
    return (
      <div>
        <h1>{this.state.value}</h1>
      </div>
    );
  }
}

export default Counter;