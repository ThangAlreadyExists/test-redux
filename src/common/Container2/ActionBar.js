import React, { Component } from 'react';
import Store from '../../store';

import Button from '../Button';

class ActionBar extends Component {
  constructor(props) {
    super(props);
    this.store = Store.getInstance();
  }

  render() {
    return (
      <div>
        <Button onAction={() => this.store.dispatch({ type: 'INCREMENT' })}>+</Button>
        <Button onAction={() => this.store.dispatch({ type: 'DECREMENT' })}>-</Button>
      </div>
    );
  }
}

export default ActionBar;