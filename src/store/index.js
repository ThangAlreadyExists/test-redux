import { createStore } from 'redux';
import counter from '../reducers';

class Store {
  static getInstance(){
    if (!this.store) {
      this.store = createStore(counter);
    }
    return this.store;
  };
}

// const Store = (function (params) {
//   let instance;
//   function createInstance() {
//     return createStore(counter);
//   }

//   return {
//     getInstance: function () {
//       if (!instance) {
//         instance = createInstance();
//       }
//       return instance;
//     }
//   }
// })();

export default Store;